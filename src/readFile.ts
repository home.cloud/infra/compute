import * as fs from 'fs'

export function readFile(filePath: string) {
  try {
    return fs.readFileSync(filePath).toString('utf8').trim()
  } catch (error) {
    if (error instanceof Error) {
      throw new Error(
        `Failed to read file: ${filePath}. Error: ${error.message}`
      )
    } else {
      throw new Error(
        `Failed to read file: ${filePath}. Unknown error: ${String(error)}`
      )
    }
  }
}
