import { execSync } from 'child_process'
import * as crypto from 'crypto'
import * as fs from 'fs'

/**
 * Renders the provision script using a shell command and returns its hash.
 * @param provisionTemplatePath - The path to the provision template file.
 * @param provisionScriptPath - The path where the rendered provision script will be saved.
 * @returns The hash of the rendered provision script or null if an error occurs.
 */
export const renderProvisionScript = (
  provisionTemplatePath: string,
  provisionScriptPath: string,
  publicKey: string
): string => {
  try {
    // Create the provision script
    process.env.SSH_KEY = publicKey
    execSync(
      `cat ${provisionTemplatePath} | envsubst >| ${provisionScriptPath}`,
      {
        env: {
          SSH_KEY: publicKey // Add your custom environment variable
        }
      }
    )

    // Calculate the hash of the rendered content
    const provisionScriptContent = fs.readFileSync(provisionScriptPath)
    const hash = crypto
      .createHash('sha256')
      .update(provisionScriptContent)
      .digest('hex')
      .substring(0, 5)

    return hash
  } catch (error) {
    console.error(`An error occurred: ${error}`)
    return ''
  }
}
