import * as crypto from 'crypto'
import * as fs from 'fs'

/**
 * @param {string} path
 * @returns {string}
 */
export const hash = (scriptPath: string): string => {
  try {
    // Create the provision script
    const scriptContent = fs.readFileSync(scriptPath)
    const hash = crypto
      .createHash('sha256')
      .update(scriptContent)
      .digest('hex')
      .substring(0, 5)

    return hash
  } catch (error) {
    console.error(`An error occurred: ${error}`)
    return ''
  }
}
