#!/bin/bash
set -x

# Redirect stdout to a log file and also display it
exec > >(tee -a snapshot.log)

# Redirect stderr to an error log file and also display it
exec 2> >(tee -a snapshot.err >&2)

# Function to check if a snapshot already exists
snapshot_exists() {
  pct snapshot $CONTAINER_ID list | grep -q "$SNAPSHOT_NAME"
}

# Function to delete a snapshot
delete_snapshot() {
  echo "Deleting existing snapshot $SNAPSHOT_NAME for container $CONTAINER_ID..."
  pct snapshot $CONTAINER_ID del $SNAPSHOT_NAME
}

# Function to create a snapshot
create_snapshot() {
  echo "Creating new snapshot $SNAPSHOT_NAME for container $CONTAINER_ID..."
  pct snapshot $CONTAINER_ID $SNAPSHOT_NAME
}

# Main script logic
if snapshot_exists; then
  delete_snapshot
fi

create_snapshot

echo "Snapshot $SNAPSHOT_NAME for container $CONTAINER_ID has been created successfully."

set +x