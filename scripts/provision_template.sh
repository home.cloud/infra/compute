#!/bin/bash

# Redirect stdout to a log file and also display it
exec > >(tee -a provision.log)

# Redirect stderr to an error log file and also display it
exec 2> >(tee -a provision.err >&2)

install_or_update_dotfiles() {
  git -C ~/.dotfiles pull || git clone https://github.com/pcuci/dotfiles.git ~/.dotfiles
  rm -f ~/.profile
  cd ~/.dotfiles && ./install
}

add_authorized_key() {
  mkdir -p ~/.ssh
  chmod 700 ~/.ssh
  grep -qxF "$SSH_KEY" ~/.ssh/authorized_keys || echo "$SSH_KEY" >>~/.ssh/authorized_keys
  chmod 600 ~/.ssh/authorized_keys
}

ansible_provision() {
  git clone https://gitlab.com/deposition.cloud/infra/compute/sun.git ~/sun
  cd ~/sun
  ansible-galaxy collection install -r requirements.yaml
  ansible-galaxy role install -r requirements.yaml --roles-path roles/
  ansible-playbook -i inventory.yaml local.yml -K --tags docker
}

# Main Script
main() {
  # Enable debugging output
  set -x

  # Update and upgrade packages
  apt-get update && apt-get upgrade -y

  # Install essential packages
  apt-get install -y --no-install-recommends sudo curl git keychain vim ansible jq

  # Add user `paul` if not exists and add to sudo group
  id -u paul &>/dev/null || adduser --home /home/paul --shell /bin/bash paul
  adduser paul sudo

  # Setup root dotfiles
  install_or_update_dotfiles

  # Add authorized key for root
  add_authorized_key

  # Ansible provision
  ansible_provision

  # Write the functions and their calls to a temporary script file
  cat <<EOF >/tmp/provision_temp.sh
$(declare -f install_or_update_dotfiles)
$(declare -f add_authorized_key)
install_or_update_dotfiles
add_authorized_key
EOF

  # Change the script permissions to make it executable
  chmod +x /tmp/provision_temp.sh

  # Execute the script as user `paul`
  sudo -u paul /tmp/provision_temp.sh

  # Delete the temporary script file
  rm -f /tmp/provision_temp.sh

  # Disable debugging output
  set +x
}

# Execute main script
main
