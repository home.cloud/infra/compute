import * as proxmoxve from '@muhlba91/pulumi-proxmoxve'
import { remote, types } from '@pulumi/command'
import * as pulumi from '@pulumi/pulumi'
import { FileAsset } from '@pulumi/pulumi/asset'
import { hash } from './src/hashFile'
import { readFile } from './src/readFile'
import { renderProvisionScript } from './src/renderProvisionScript'

const org = pulumi.getOrganization()
const project = pulumi.getProject()
const stack = pulumi.getStack()

const templateName = `${org}-${stack}-${project}-noble-2404`

const config = new pulumi.Config()

const endpoint = config.require('endpoint')
const username = config.require('username')
const password = config.requireSecret('password')
const host = config.require('host')
const nodeName = config.require('nodeName')

const privateKeyPath = config.require('privateKeyPath')
const publicKeyPath = config.require('publicKeyPath')

const privateKey = readFile(privateKeyPath)
const publicKey = readFile(publicKeyPath)

const provisionTemplatePath = 'scripts/provision_template.sh'
const provisionScriptPath = 'scripts/provision.sh'

// Render the provision template and return the new file's hash
const provisionScriptHash: string = renderProvisionScript(
  provisionTemplatePath,
  provisionScriptPath,
  publicKey
)

const providerArgs: proxmoxve.ProviderArgs = {
  endpoint,
  username,
  password,
  insecure: true
}

const proxmoxveProvider = new proxmoxve.Provider('proxmoxve', providerArgs)
const disk = {
  datastoreId: 'local-lvm',
  size: 10
}

const template = new proxmoxve.ct.Container(
  'template',
  {
    nodeName,
    started: true,
    unprivileged: true,
    operatingSystem: {
      type: 'ubuntu',
      templateFileId: 'local:vztmpl/ubuntu-24.04-standard_24.04-2_amd64.tar.zst'
    },
    cpu: {
      cores: 4
    },
    disk,
    memory: {
      dedicated: 2048
    },
    networkInterfaces: [
      {
        name: 'eth0',
        bridge: 'vmbr0'
      }
    ],
    features: {
      nesting: true
    },
    tags: [stack, org],
    initialization: {
      userAccount: {
        password,
        keys: [publicKey]
      },
      hostname: templateName,
      dns: {
        domain: 'lan',
        servers: ['192.168.1.1']
      },
      ipConfigs: [
        {
          ipv4: {
            address: 'dhcp'
          }
        }
      ]
    }
  },
  {
    provider: proxmoxveProvider
  }
)

const pveConnection: types.input.remote.ConnectionArgs = {
  host,
  user: 'root',
  privateKey: pulumi.secret(privateKey),
  privateKeyPassword: password,
  dialErrorLimit: 30
}

const ip = new remote.Command(
  'ip' ,
  {
    connection: pveConnection,
    create: pulumi.interpolate`sleep 3 && pct exec ${template.id} -- ip -4 a show eth0 | grep inet | awk '{print $2}' | cut -d'/' -f1`
  },
  {
    parent: template,
    dependsOn: template
  }
)

const templateConnection: types.input.remote.ConnectionArgs = {
  ...pveConnection,
  host: pulumi.interpolate`${ip.stdout}`
}

// Re-copy the provision file to the container every time it changes
const provisionScript = new remote.CopyToRemote(
  `provisionScript-${provisionScriptHash}`,
  {
    connection: templateConnection,
    source: new FileAsset(provisionScriptPath),
    remotePath: 'provision.sh'
  },
  {
    parent: ip,
    dependsOn: ip
  }
)

const provision = new remote.Command(
  'provision',
  {
    connection: templateConnection,
    create: 'bash ~/provision.sh',
    triggers: [provisionScript.id]
  },
  {
    parent: provisionScript,
    dependsOn: provisionScript
  }
)

const snapshotScriptPath = 'scripts/snapshot.sh'

const snapshotScript = new remote.CopyToRemote(
  `snapshotScript-${hash(snapshotScriptPath)}`,
  {
    connection: pveConnection,
    source: new FileAsset(snapshotScriptPath),
    remotePath: 'snapshot.sh'
  },
  {
    parent: provision,
    dependsOn: provision
  }
)

const snapshot = new remote.Command(
  'snapshot',
  {
    connection: pveConnection,
    create: pulumi.interpolate`
    export CONTAINER_ID=${template.id}
    export SNAPSHOT_NAME="pulumi-snapshot"
    bash ~/snapshot.sh`,
    triggers: [snapshotScript.id]
  },
  {
    parent: snapshotScript,
    dependsOn: snapshotScript
  }
)

new remote.Command(
  'stop',
  {
    connection: pveConnection,
    create: pulumi.interpolate`pct stop ${template.id}`,
    triggers: [snapshotScript.id]
  },
  {
    parent: snapshot,
    dependsOn: snapshot
  }
)

export const out = {
  providerArgs,
  node: {
    name: nodeName,
    host: host
  },
  template: {
    id: template.id.apply((id: string) => +id || 666),
    ip: pulumi.unsecret(ip.stdout),
    disk
  },
  pveConnection
}
